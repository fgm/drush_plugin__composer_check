# Drupal Composer check

This Drush plugin provides a `composer:check` (alias `cch`) command, which
compares the packages versions in `composer.lock` with those requested in the
`composer.json` file of a Drupal 9 project.


## Syntax

* Arguments :
  * `composer.lock`: Optional. The path to the lock file. Defaults to the one
    above the project root, which is the default in modern Composer layouts for
    Drupal 9.
* Options :
  * `--all`: List all locked packages, even those not requested
  * `--yaml`: Produce YAML output instead of a table
* Aliases: `cch`


# Composer / Packagist

This package is available on [Packagist] as `fgm/composer-check`.

[Packagist]: https://packagist.org/packages/fgm/composer-check


# Examples

* Default usage in a `drupal/core-recommended` or `drupal-composer/drupal-project` project :

      drush cch

* Listing all packages in a legacy `drupal/drupal` project, in YAML format :

      drush cch --all --yaml $PWD/composer.lock


# License

Like any Drupal code, this plugin is licensed under the General Public License,
version 2.0 or later.

For jurisdictions needing it, it is also licensed under the CeCILL 2.1 or later.
